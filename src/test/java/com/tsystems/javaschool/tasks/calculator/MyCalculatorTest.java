package com.tsystems.javaschool.tasks.calculator;

import org.junit.Assert;
import org.junit.Test;

public class MyCalculatorTest {
    private Calculator calc = new Calculator();

    @Test
    public void evaluate() {
        //given
        String input = "15 + 12   / 6 - (16  - 5 )";
        String expectedResult = "6";

        //run
        String result = calc.evaluate(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void evaluate1() {
        //given
        String input = " -1 + 1 + 2 * 45 /45 - 1 + 45 ";
        String expectedResult = "46";

        //run
        String result = calc.evaluate(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }
    @Test
    public void evaluate2() {
        //given
        String input = "-( -1 +21) *1 /1";
        String expectedResult = "-20";

        //run
        String result = calc.evaluate(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

}
