package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            inputNumbers.sort(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {

                    return o1 - o2;
                }
            });
        } catch (java.lang.OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
        int inc = 1;
        int sum = 0;
        while (inputNumbers.size() > sum) {
            sum += inc;
            if (sum == inputNumbers.size()) {
                break;
            }
            inc++;
            if (sum > inputNumbers.size()) {
                throw new CannotBuildPyramidException();
            }
        }
        if (inc == 1) {
            int[][] result = new int[1][1];
            result[0][0] = inputNumbers.get(0);
            return result;
        }
        int[][] ints = new int[inc][inc * 2 - 1];
        int index = 0;
        for (int i = 1; i <= inc; i++) {
            createLine(ints, i - 1, inc - 1, inputNumbers.subList(index, index + i));
            index += i;
        }
        return ints;
    }

    private void createLine(int[][] arr, int line, int med, List<Integer> inputNumber) {
        if (inputNumber.size() % 2 == 1) {
            arr[line][med] = inputNumber.get((inputNumber.size() - 1) / 2);
            for (int i = 1; i <= (inputNumber.size() - 1) / 2; i++) {
                arr[line][med - i * 2] = inputNumber.get((inputNumber.size() - 1) / 2 - i);
                arr[line][med + i * 2] = inputNumber.get((inputNumber.size() - 1) / 2 + i);
            }
            return;
        }
        {
            for (int i = 1; i <= inputNumber.size() / 2; i++) {
                arr[line][med - i * 2 + 1] = inputNumber.get((inputNumber.size()) / 2 - i);
                arr[line][med + i * 2 - 1] = inputNumber.get((inputNumber.size()) / 2 + i - 1);
            }
        }
    }

}
